﻿using FarPoint.Win.Spread;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SpreadDesigner
{
  public partial class SpreadOptionsDlg : Form
  {

    public SpreadOptionsDlg()
    {
      base.Load += new EventHandler(this.SpreadOptions_Load);
      base.KeyDown += new KeyEventHandler(this.SpreadOptionsDlg_KeyDown);
      this.helpFile = null;
      this.topic = null;
      this.InitializeComponent();
      this.initializeTitleTab();
      this.initializeEditButton();
    }

    public SpreadOptionsDlg(FpSpread spread)
    {
      base.Load += new EventHandler(this.SpreadOptions_Load);
      base.KeyDown += new KeyEventHandler(this.SpreadOptionsDlg_KeyDown);
      this.helpFile = null;
      this.topic = null;
      this.spread = spread;
      this.InitializeComponent();
      this.initializeTitleTab();
      this.initializeEditButton();
    }

    public SpreadOptionsDlg(FpSpread spread, int tabIndex)
    {
      base.Load += new EventHandler(this.SpreadOptions_Load);
      base.KeyDown += new KeyEventHandler(this.SpreadOptionsDlg_KeyDown);
      this.helpFile = null;
      this.topic = null;
      this.spread = spread;
      this.InitializeComponent();
      this.initializeTitleTab();
      this.initializeEditButton();
      this.TabControl1.SelectedIndex = tabIndex;
    }

    public SpreadOptionsDlg(FpSpread spread, string helpFile, string topic)
    {
      base.Load += new EventHandler(this.SpreadOptions_Load);
      base.KeyDown += new KeyEventHandler(this.SpreadOptionsDlg_KeyDown);
      this.helpFile = null;
      this.topic = null;
      this.spread = spread;
      this.helpFile = helpFile;
      this.topic = topic;
      this.InitializeComponent();
      this.initializeTitleTab();
      this.initializeEditButton();
      if ((helpFile == null) | (topic == null))
      {
        this.spOpt_Help.Enabled = false;
      }
    }

    private void Cancel_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Cancel;
    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (this.components != null))
      {
        this.components.Dispose();
      }
      base.Dispose(disposing);
    }

    private void ExchangeBoolProp(bool isInit, ref bool val, CheckBox checkBox)
    {
      if (isInit)
      {
        checkBox.Checked = val;
      }
      else
      {
        val = checkBox.Checked;
      }
    }

    private void ExchangeEnumProp(bool isInit, ref int val, object button1, int buttonVal1)
    {
      EnumButtonList[] list = new EnumButtonList[2];
      list[0].button = button1;
      list[0].buttonVal = buttonVal1;
      this.ExchangeEnumProp(isInit, ref val, list, 1);
    }

    private void ExchangeEnumProp(bool isInit, ref int val, EnumButtonList[] list, int listCount)
    {
      int i;
      if (isInit) // We are loading the form
      {
        for (i = 0; i < listCount; i++)
        {
          if (list[i].button is RadioButton)
            if (val == list[i].buttonVal)
              ((RadioButton)list[i].button).Checked = true;
            else
              ((RadioButton)list[i].button).Checked = false;
          else
            if (list[i].buttonVal == 0)
              if (val == 0)
                ((CheckBox)list[i].button).Checked = true;
              else
                ((CheckBox)list[i].button).Checked = false;
            else if ((val & list[i].buttonVal) == list[i].buttonVal)
              ((CheckBox)list[i].button).Checked = true;
            else
              ((CheckBox)list[i].button).Checked = false;
        }
      }
      else // We are applying the changes
      {
        val = 0;
        for (i = 0; i < listCount; i++)
        {
          if (list[i].button is RadioButton)
          {
            if (((RadioButton)list[i].button).Checked)
              val = (val | list[i].buttonVal);
          }
          else if (list[i].button is CheckBox)
            if (((CheckBox)list[i].button).Checked)
              val = (val | list[i].buttonVal);
        }
      }
    }

    private void ExchangeEnumProp(bool isInit, ref int val, object button1, int buttonVal1, object button2, int buttonVal2)
    {
      EnumButtonList[] list = new EnumButtonList[3];
      list[0].button = button1;
      list[0].buttonVal = buttonVal1;
      list[1].button = button2;
      list[1].buttonVal = buttonVal2;
      this.ExchangeEnumProp(isInit, ref val, list, 2);
    }

    private void ExchangeEnumProp(bool isInit, ref int val, object button1, int buttonVal1, object button2, int buttonVal2, object button3, int buttonVal3)
    {
      EnumButtonList[] list = new EnumButtonList[4];
      list[0].button = button1;
      list[0].buttonVal = buttonVal1;
      list[1].button = button2;
      list[1].buttonVal = buttonVal2;
      list[2].button = button3;
      list[2].buttonVal = buttonVal3;
      this.ExchangeEnumProp(isInit, ref val, list, 3);
    }

    private void ExchangeEnumProp(bool isInit, ref int val, object button1, int buttonVal1, object button2, int buttonVal2, object button3, int buttonVal3, object button4, int buttonVal4)
    {
      EnumButtonList[] list = new EnumButtonList[5];
      list[0].button = button1;
      list[0].buttonVal = buttonVal1;
      list[1].button = button2;
      list[1].buttonVal = buttonVal2;
      list[2].button = button3;
      list[2].buttonVal = buttonVal3;
      list[3].button = button4;
      list[3].buttonVal = buttonVal4;
      this.ExchangeEnumProp(isInit, ref val, list, 4);
    }

    private void ExchangeEnumProp(bool isInit, ref int val, object button1, int buttonVal1, object button2, int buttonVal2, object button3, int buttonVal3, object button4, int buttonVal4, object button5, int buttonVal5)
    {
      EnumButtonList[] list = new EnumButtonList[6];
      list[0].button = button1;
      list[0].buttonVal = buttonVal1;
      list[1].button = button2;
      list[1].buttonVal = buttonVal2;
      list[2].button = button3;
      list[2].buttonVal = buttonVal3;
      list[3].button = button4;
      list[3].buttonVal = buttonVal4;
      list[4].button = button5;
      list[4].buttonVal = buttonVal5;
      this.ExchangeEnumProp(isInit, ref val, list, 5);
    }

    private void ExchangeEnumProp(bool isInit, ref int val, object button1, int buttonVal1, object button2, int buttonVal2, object button3, int buttonVal3, object button4, int buttonVal4, object button5, int buttonVal5, object button6, int buttonVal6)
    {
      EnumButtonList[] list = new EnumButtonList[7];
      list[0].button = button1;
      list[0].buttonVal = buttonVal1;
      list[1].button = button2;
      list[1].buttonVal = buttonVal2;
      list[2].button = button3;
      list[2].buttonVal = buttonVal3;
      list[3].button = button4;
      list[3].buttonVal = buttonVal4;
      list[4].button = button5;
      list[4].buttonVal = buttonVal5;
      list[5].button = button6;
      list[5].buttonVal = buttonVal6;
      this.ExchangeEnumProp(isInit, ref val, list, 6);
    }

    private void ExchangeEnumProp(bool isInit, ref int val, object button1, int buttonVal1, object button2, int buttonVal2, object button3, int buttonVal3, object button4, int buttonVal4, object button5, int buttonVal5, object button6, int buttonVal6, object button7, int buttonVal7)
    {
      EnumButtonList[] list = new EnumButtonList[8];
      list[0].button = button1;
      list[0].buttonVal = buttonVal1;
      list[1].button = button2;
      list[1].buttonVal = buttonVal2;
      list[2].button = button3;
      list[2].buttonVal = buttonVal3;
      list[3].button = button4;
      list[3].buttonVal = buttonVal4;
      list[4].button = button5;
      list[4].buttonVal = buttonVal5;
      list[5].button = button6;
      list[5].buttonVal = buttonVal6;
      list[6].button = button7;
      list[6].buttonVal = buttonVal7;
      this.ExchangeEnumProp(isInit, ref val, list, 7);
    }

    private void ExchangeStringProp(bool isInit, ref string val, TextBox textBox)
    {
      if (isInit)
      {
        textBox.Text = val;
      }
      else
      {
        val = textBox.Text;
      }
    }

    private void FormExchange(bool isInit)
    {
      FpSpread spread;
      bool allowEditOverflow;
      int clipboardOptions;
      TitleInfo titleInfo;
      string text;
      if (this.spread is FpSpread)
      {
        spread = this.spread;
        allowEditOverflow = spread.AllowEditOverflow;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_Edit_AllowEditOverflow);
        spread.AllowEditOverflow = allowEditOverflow;
        spread = this.spread;
        allowEditOverflow = spread.EditModePermanent;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_Edit_EditModePermanent);
        spread.EditModePermanent = allowEditOverflow;
        spread = this.spread;
        allowEditOverflow = spread.EditModeReplace;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_Edit_EditModeReplace);
        spread.EditModeReplace = allowEditOverflow;
        spread = this.spread;
        allowEditOverflow = spread.MoveActiveOnFocus;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_Edit_MoveActiveOnFocus);
        spread.MoveActiveOnFocus = allowEditOverflow;
        spread = this.spread;
        allowEditOverflow = spread.AllowDragDrop;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_Gen_AllowDragDrop);
        spread.AllowDragDrop = allowEditOverflow;
        spread = this.spread;
        allowEditOverflow = spread.AllowDragFill;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_Gen_AllowDragFill);
        spread.AllowDragFill = allowEditOverflow;
        spread = this.spread;
        allowEditOverflow = spread.AllowUserFormulas;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_Gen_AllowUserFormulas);
        spread.AllowUserFormulas = allowEditOverflow;
        spread = this.spread;
        allowEditOverflow = spread.AutoClipboard;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_Gen_AutoClipboard);
        spread.AutoClipboard = allowEditOverflow;
        spread = this.spread;
        clipboardOptions = (int)spread.ClipboardOptions;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_Gen_NoHeaders, 0, this.spOpt_Gen_RowHeaders, 1, this.spOpt_Gen_ColumnHeaders, 2, this.spOpt_Gen_AllHeaders, 3);
        spread.ClipboardOptions = (ClipboardOptions)clipboardOptions;
        spread = this.spread;
        clipboardOptions = (int)spread.TabStripPolicy;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_Gen_TabStrip_Always, 0, this.spOpt_Gen_TabStrip_Never, 1, this.spOpt_Gen_TabStrip_AsNeeded, 2);
        spread.TabStripPolicy = (TabStripPolicy)clipboardOptions;
        if (isInit)
        {
          this.spOpt_Gen_TabStripRatio.Value = new decimal(this.spread.TabStripRatio * 100.0);
        }
        else
        {
          this.spread.TabStripRatio = Convert.ToDouble(decimal.Divide(this.spOpt_Gen_TabStripRatio.Value, 100M));
        }
        spread = this.spread;
        allowEditOverflow = spread.ScrollBarMaxAlign;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_SB_ScrollBarMaxAlign);
        spread.ScrollBarMaxAlign = allowEditOverflow;
        spread = this.spread;
        allowEditOverflow = spread.ScrollBarShowMax;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_SB_ScrollBarShowMax);
        spread.ScrollBarShowMax = allowEditOverflow;
        spread = this.spread;
        clipboardOptions = (int)spread.HorizontalScrollBarPolicy;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_SB_HSB_Always, 1, this.spOpt_SB_HSB_Never, 2, this.spOpt_SB_HSB_AsNeeded, 0);
        spread.HorizontalScrollBarPolicy = (ScrollBarPolicy)clipboardOptions;
        spread = this.spread;
        clipboardOptions = (int)spread.VerticalScrollBarPolicy;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_SB_VSB_Always, 1, this.spOpt_SB_VSB_Never, 2, this.spOpt_SB_VSB_AsNeeded, 0);
        spread.VerticalScrollBarPolicy = (ScrollBarPolicy)clipboardOptions;
        spread = this.spread;
        clipboardOptions = (int)spread.ScrollBarTrackPolicy;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_SB_SBTrack_Vertical, 1, this.spOpt_SB_SBTrack_Horizontal, 2);
        spread.ScrollBarTrackPolicy = (ScrollBarTrackPolicy)clipboardOptions;
        spread = this.spread;
        clipboardOptions = (int)spread.ScrollTipPolicy;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_SB_ScrollTip_Vertical, 1, this.spOpt_SB_ScrollTip_Horizontal, 2);
        spread.ScrollTipPolicy = (ScrollTipPolicy)clipboardOptions;
        spread = this.spread;
        clipboardOptions = (int)spread.ColumnSplitBoxAlignment;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_SplitBox_Col_Leading, 0, this.spOpt_SplitBox_Col_Trailing, 1);
        spread.ColumnSplitBoxAlignment = (SplitBoxAlignment)clipboardOptions;
        spread = this.spread;
        clipboardOptions = (int)spread.ColumnSplitBoxPolicy;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_SplitBox_Col_Always, 0, this.spOpt_SplitBox_Col_AsNeeded, 2, this.spOpt_SplitBox_Col_Never, 1);
        spread.ColumnSplitBoxPolicy = (SplitBoxPolicy)clipboardOptions;
        spread = this.spread;
        clipboardOptions = (int)spread.RowSplitBoxAlignment;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_SplitBox_Row_Leading, 0, this.spOpt_SplitBox_Row_Trailing, 1);
        spread.RowSplitBoxAlignment = (SplitBoxAlignment)clipboardOptions;
        spread = this.spread;
        clipboardOptions = (int)spread.RowSplitBoxPolicy;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_SplitBox_Row_Always, 0, this.spOpt_SplitBox_Row_AsNeeded, 2, this.spOpt_SplitBox_Row_Never, 1);
        spread.RowSplitBoxPolicy = (SplitBoxPolicy)clipboardOptions;
        spread = this.spread;
        allowEditOverflow = spread.AllowCellOverflow;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_View_AllowCellOverflow);
        spread.AllowCellOverflow = allowEditOverflow;
        spread = this.spread;
        allowEditOverflow = spread.CellNoteIndicatorVisible;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_View_CellNoteIndicatorVisible);
        spread.CellNoteIndicatorVisible = allowEditOverflow;
        spread = this.spread;
        allowEditOverflow = spread.RetainSelectionBlock;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_View_RetainSelectionBlock);
        spread.RetainSelectionBlock = allowEditOverflow;
        spread = this.spread;
        clipboardOptions = (int)spread.ButtonDrawMode;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_View_ButtonDrawMode_Always, 0, this.spOpt_View_ButtonDrawMode_CurrentCell, 1, this.spOpt_View_ButtonDrawMode_CurrentColumn, 2, this.spOpt_View_ButtonDrawMode_CurrentRow, 4, this.spOpt_View_ButtonDrawMode_AlwaysPrimary, 8, this.spOpt_View_ButtonDrawMode_AlwaysSecondary, 0x10, this.spOpt_View_ButtonDrawMode_AlwaysEditButton, 0x20);
        spread.ButtonDrawMode = (ButtonDrawModes)clipboardOptions;
        spread = this.spread;
        clipboardOptions = (int)spread.SelectionBlockOptions;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_View_SelectBlockOptions_Cells, 1, this.spOpt_View_SelectBlockOptions_Rows, 2, this.spOpt_View_SelectBlockOptions_Columns, 4, this.spOpt_View_SelectBlockOptions_Sheet, 8);
        spread.SelectionBlockOptions = (SelectionBlockOptions)clipboardOptions;
        titleInfo = this.spread.TitleInfo;
        allowEditOverflow = titleInfo.Visible;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.chbShowTitle);
        titleInfo.Visible = allowEditOverflow;
        titleInfo = this.spread.TitleInfo;
        text = titleInfo.Text;
        this.ExchangeStringProp(isInit, ref text, this.txtTitleText);
        titleInfo.Text = text;
        if (this.spread.ActiveSheet != null)
        {
          titleInfo = this.spread.ActiveSheet.TitleInfo;
          allowEditOverflow = titleInfo.Visible;
          this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.chbShowSubtitle);
          titleInfo.Visible = allowEditOverflow;
          titleInfo = this.spread.ActiveSheet.TitleInfo;
          text = titleInfo.Text;
          this.ExchangeStringProp(isInit, ref text, this.txtSubtitleText);
          titleInfo.Text = text;
        }
      }
      else
      {
        spread = this.spread;
        allowEditOverflow = spread.AllowEditOverflow;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_Edit_AllowEditOverflow);
        spread.AllowEditOverflow = allowEditOverflow;
        spread = this.spread;
        allowEditOverflow = spread.EditModePermanent;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_Edit_EditModePermanent);
        spread.EditModePermanent = allowEditOverflow;
        spread = this.spread;
        allowEditOverflow = spread.EditModeReplace;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_Edit_EditModeReplace);
        spread.EditModeReplace = allowEditOverflow;
        spread = this.spread;
        allowEditOverflow = spread.MoveActiveOnFocus;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_Edit_MoveActiveOnFocus);
        spread.MoveActiveOnFocus = allowEditOverflow;
        spread = this.spread;
        allowEditOverflow = spread.AllowDragDrop;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_Gen_AllowDragDrop);
        spread.AllowDragDrop = allowEditOverflow;
        spread = this.spread;
        allowEditOverflow = spread.AllowDragFill;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_Gen_AllowDragFill);
        spread.AllowDragFill = allowEditOverflow;
        spread = this.spread;
        allowEditOverflow = spread.AllowUserFormulas;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_Gen_AllowUserFormulas);
        spread.AllowUserFormulas = allowEditOverflow;
        spread = this.spread;
        allowEditOverflow = spread.AutoClipboard;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_Gen_AutoClipboard);
        spread.AutoClipboard = allowEditOverflow;
        spread = this.spread;
        clipboardOptions = (int)spread.ClipboardOptions;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_Gen_NoHeaders, 0, this.spOpt_Gen_RowHeaders, 1, this.spOpt_Gen_ColumnHeaders, 2, this.spOpt_Gen_AllHeaders, 3);
        spread.ClipboardOptions = (ClipboardOptions)clipboardOptions;
        spread = this.spread;
        clipboardOptions = (int)spread.TabStripPolicy;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_Gen_TabStrip_Always, 0, this.spOpt_Gen_TabStrip_Never, 1, this.spOpt_Gen_TabStrip_AsNeeded, 2);
        spread.TabStripPolicy = (TabStripPolicy)clipboardOptions;
        if (isInit)
        {
          this.spOpt_Gen_TabStripRatio.Value = new decimal(this.spread.TabStripRatio * 100.0);
        }
        else
        {
          this.spread.TabStripRatio = Convert.ToDouble(decimal.Divide(this.spOpt_Gen_TabStripRatio.Value, 100M));
        }
        spread = this.spread;
        allowEditOverflow = spread.ScrollBarMaxAlign;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_SB_ScrollBarMaxAlign);
        spread.ScrollBarMaxAlign = allowEditOverflow;
        spread = this.spread;
        allowEditOverflow = spread.ScrollBarShowMax;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_SB_ScrollBarShowMax);
        spread.ScrollBarShowMax = allowEditOverflow;
        spread = this.spread;
        clipboardOptions = (int)spread.HorizontalScrollBarPolicy;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_SB_HSB_Always, 1, this.spOpt_SB_HSB_Never, 2, this.spOpt_SB_HSB_AsNeeded, 0);
        spread.HorizontalScrollBarPolicy = (ScrollBarPolicy)clipboardOptions;
        spread = this.spread;
        clipboardOptions = (int)spread.VerticalScrollBarPolicy;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_SB_VSB_Always, 1, this.spOpt_SB_VSB_Never, 2, this.spOpt_SB_VSB_AsNeeded, 0);
        spread.VerticalScrollBarPolicy = (ScrollBarPolicy)clipboardOptions;
        spread = this.spread;
        clipboardOptions = (int)spread.ScrollBarTrackPolicy;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_SB_SBTrack_Vertical, 1, this.spOpt_SB_SBTrack_Horizontal, 2);
        spread.ScrollBarTrackPolicy = (ScrollBarTrackPolicy)clipboardOptions;
        spread = this.spread;
        clipboardOptions = (int)spread.ScrollTipPolicy;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_SB_ScrollTip_Vertical, 1, this.spOpt_SB_ScrollTip_Horizontal, 2);
        spread.ScrollTipPolicy = (ScrollTipPolicy)clipboardOptions;
        spread = this.spread;
        clipboardOptions = (int)spread.ColumnSplitBoxAlignment;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_SplitBox_Col_Leading, 0, this.spOpt_SplitBox_Col_Trailing, 1);
        spread.ColumnSplitBoxAlignment = (SplitBoxAlignment)clipboardOptions;
        spread = this.spread;
        clipboardOptions = (int)spread.ColumnSplitBoxPolicy;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_SplitBox_Col_Always, 0, this.spOpt_SplitBox_Col_AsNeeded, 2, this.spOpt_SplitBox_Col_Never, 1);
        spread.ColumnSplitBoxPolicy = (SplitBoxPolicy)clipboardOptions;
        spread = this.spread;
        clipboardOptions = (int)spread.RowSplitBoxAlignment;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_SplitBox_Row_Leading, 0, this.spOpt_SplitBox_Row_Trailing, 1);
        spread.RowSplitBoxAlignment = (SplitBoxAlignment)clipboardOptions;
        spread = this.spread;
        clipboardOptions = (int)spread.RowSplitBoxPolicy;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_SplitBox_Row_Always, 0, this.spOpt_SplitBox_Row_AsNeeded, 2, this.spOpt_SplitBox_Row_Never, 1);
        spread.RowSplitBoxPolicy = (SplitBoxPolicy)clipboardOptions;
        spread = this.spread;
        allowEditOverflow = spread.AllowCellOverflow;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_View_AllowCellOverflow);
        spread.AllowCellOverflow = allowEditOverflow;
        spread = this.spread;
        allowEditOverflow = spread.CellNoteIndicatorVisible;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_View_CellNoteIndicatorVisible);
        spread.CellNoteIndicatorVisible = allowEditOverflow;
        spread = this.spread;
        allowEditOverflow = spread.RetainSelectionBlock;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.spOpt_View_RetainSelectionBlock);
        spread.RetainSelectionBlock = allowEditOverflow;
        spread = this.spread;
        clipboardOptions = (int)spread.ButtonDrawMode;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_View_ButtonDrawMode_Always, 0, this.spOpt_View_ButtonDrawMode_CurrentCell, 1, this.spOpt_View_ButtonDrawMode_CurrentColumn, 2, this.spOpt_View_ButtonDrawMode_CurrentRow, 4, this.spOpt_View_ButtonDrawMode_AlwaysPrimary, 8, this.spOpt_View_ButtonDrawMode_AlwaysSecondary, 0x10, this.spOpt_View_ButtonDrawMode_AlwaysEditButton, 0x20);
        spread.ButtonDrawMode = (ButtonDrawModes)clipboardOptions;
        spread = this.spread;
        clipboardOptions = (int)spread.SelectionBlockOptions;
        this.ExchangeEnumProp(isInit, ref clipboardOptions, this.spOpt_View_SelectBlockOptions_Cells, 1, this.spOpt_View_SelectBlockOptions_Rows, 2, this.spOpt_View_SelectBlockOptions_Columns, 4, this.spOpt_View_SelectBlockOptions_Sheet, 8);
        spread.SelectionBlockOptions = (SelectionBlockOptions)clipboardOptions;
        titleInfo = this.spread.TitleInfo;
        allowEditOverflow = titleInfo.Visible;
        this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.chbShowTitle);
        titleInfo.Visible = allowEditOverflow;
        titleInfo = this.spread.TitleInfo;
        text = titleInfo.Text;
        this.ExchangeStringProp(isInit, ref text, this.txtTitleText);
        titleInfo.Text = text;
        if (this.spread.ActiveSheet != null)
        {
          titleInfo = this.spread.ActiveSheet.TitleInfo;
          allowEditOverflow = titleInfo.Visible;
          this.ExchangeBoolProp(isInit, ref allowEditOverflow, this.chbShowSubtitle);
          titleInfo.Visible = allowEditOverflow;
          titleInfo = this.spread.ActiveSheet.TitleInfo;
          text = titleInfo.Text;
          this.ExchangeStringProp(isInit, ref text, this.txtSubtitleText);
          titleInfo.Text = text;
        }
      }
    }

    private void initializeEditButton()
    {
      this.spOpt_View_ButtonDrawMode_AlwaysEditButton = new CheckBox();
      this.spOpt_View_ButtonDrawMode_AlwaysEditButton.ImeMode = ImeMode.NoControl;
      Point point2 = new Point(0x124, 210);
      this.spOpt_View_ButtonDrawMode_AlwaysEditButton.Location = point2;
      this.spOpt_View_ButtonDrawMode_AlwaysEditButton.Name = "spOpt_View_ButtonDrawMode_AlwaysEditButton";
      Size size2 = new Size(0xef, 0x16);
      this.spOpt_View_ButtonDrawMode_AlwaysEditButton.Size = size2;
      this.spOpt_View_ButtonDrawMode_AlwaysEditButton.TabIndex = 11;
      this.spOpt_View_ButtonDrawMode_AlwaysEditButton.Text = "Always E&dit Button";
      this.spOpt_View_ButtonDrawMode_AlwaysEditButton.CheckedChanged += this.spOpt_View_ButtonDrawMode_EditMode_CheckedChanged;
      this.spOpt_TabView.Controls.Add(this.spOpt_View_ButtonDrawMode_AlwaysEditButton);
    }

    private void initializeTitleTab()
    {
      this.spOpt_TabTitle = new TabPage();
      this.gbSubtitle = new GroupBox();
      this.txtSubtitleText = new TextBox();
      this.label3 = new Label();
      this.chbShowSubtitle = new CheckBox();
      this.gbTitle = new GroupBox();
      this.txtTitleText = new TextBox();
      this.label2 = new Label();
      this.chbShowTitle = new CheckBox();
      this.spOpt_TabTitle.SuspendLayout();
      this.gbSubtitle.SuspendLayout();
      this.gbTitle.SuspendLayout();
      this.TabControl1.Controls.Add(this.spOpt_TabTitle);
      this.spOpt_TabTitle.Controls.Add(this.gbSubtitle);
      this.spOpt_TabTitle.Controls.Add(this.gbTitle);
      Point point2 = new Point(4, 0x16);
      this.spOpt_TabTitle.Location = point2;
      this.spOpt_TabTitle.Name = "spOpt_TabTitle";
      Padding padding2 = new Padding(3);
      this.spOpt_TabTitle.Padding = padding2;
      Size size2 = new Size(0x22c, 330);
      this.spOpt_TabTitle.Size = size2;
      this.spOpt_TabTitle.TabIndex = 5;
      this.spOpt_TabTitle.Text = "Titles";
      this.spOpt_TabTitle.UseVisualStyleBackColor = true;
      this.gbSubtitle.Controls.Add(this.txtSubtitleText);
      this.gbSubtitle.Controls.Add(this.label3);
      this.gbSubtitle.Controls.Add(this.chbShowSubtitle);
      point2 = new Point(14, 0x75);
      this.gbSubtitle.Location = point2;
      this.gbSubtitle.Name = "gbSubtitle";
      padding2 = new Padding(10);
      this.gbSubtitle.Padding = padding2;
      size2 = new Size(0x210, 0x53);
      this.gbSubtitle.Size = size2;
      this.gbSubtitle.TabIndex = 3;
      this.gbSubtitle.TabStop = false;
      this.gbSubtitle.Text = "Subtitle";
      point2 = new Point(0x30, 0x2f);
      this.txtSubtitleText.Location = point2;
      this.txtSubtitleText.Name = "txtSubtitleText";
      size2 = new Size(0x1b0, 20);
      this.txtSubtitleText.Size = size2;
      this.txtSubtitleText.TabIndex = 2;
      this.label3.AutoSize = true;
      point2 = new Point(13, 50);
      this.label3.Location = point2;
      this.label3.Name = "label3";
      size2 = new Size(0x1c, 13);
      this.label3.Size = size2;
      this.label3.TabIndex = 1;
      this.label3.Text = "T&ext";
      this.chbShowSubtitle.AutoSize = true;
      point2 = new Point(13, 0x1a);
      this.chbShowSubtitle.Location = point2;
      this.chbShowSubtitle.Name = "chbShowSubtitle";
      size2 = new Size(0x5b, 0x11);
      this.chbShowSubtitle.Size = size2;
      this.chbShowSubtitle.TabIndex = 0;
      this.chbShowSubtitle.Text = "S&how Subtitle";
      this.chbShowSubtitle.UseVisualStyleBackColor = true;
      this.gbTitle.Controls.Add(this.txtTitleText);
      this.gbTitle.Controls.Add(this.label2);
      this.gbTitle.Controls.Add(this.chbShowTitle);
      point2 = new Point(14, 14);
      this.gbTitle.Location = point2;
      this.gbTitle.Name = "gbTitle";
      padding2 = new Padding(10);
      this.gbTitle.Padding = padding2;
      size2 = new Size(0x210, 0x53);
      this.gbTitle.Size = size2;
      this.gbTitle.TabIndex = 2;
      this.gbTitle.TabStop = false;
      this.gbTitle.Text = "Title";
      point2 = new Point(0x30, 0x2f);
      this.txtTitleText.Location = point2;
      this.txtTitleText.Name = "txtTitleText";
      size2 = new Size(0x1b0, 20);
      this.txtTitleText.Size = size2;
      this.txtTitleText.TabIndex = 2;
      this.label2.AutoSize = true;
      point2 = new Point(13, 50);
      this.label2.Location = point2;
      this.label2.Name = "label2";
      size2 = new Size(0x1c, 13);
      this.label2.Size = size2;
      this.label2.TabIndex = 1;
      this.label2.Text = "&Text";
      this.chbShowTitle.AutoSize = true;
      point2 = new Point(13, 0x1a);
      this.chbShowTitle.Location = point2;
      this.chbShowTitle.Name = "chbShowTitle";
      size2 = new Size(0x4c, 0x11);
      this.chbShowTitle.Size = size2;
      this.chbShowTitle.TabIndex = 0;
      this.chbShowTitle.Text = "&Show Title";
      this.chbShowTitle.UseVisualStyleBackColor = true;
      this.spOpt_TabTitle.ResumeLayout(false);
      this.gbSubtitle.ResumeLayout(false);
      this.gbSubtitle.PerformLayout();
      this.gbTitle.ResumeLayout(false);
      this.gbTitle.PerformLayout();
      this.ResumeLayout(false);
    }

    private void Label_Paint(object sender, PaintEventArgs e)
    {
      common.PaintSeparator(sender, e);
    }

    private void OK_Click(object sender, EventArgs e)
    {
      this.FormExchange(false);
      this.DialogResult = DialogResult.OK;
    }

    private void spOpt_Help_Click(object sender, EventArgs e)
    {
      if (this.spread is FpSpread)
      {
        common.DisplayHelp(this, "SDSpreadDialog.html");
      }
      else if (!((this.helpFile == null) | (this.topic == null)))
      {
        Help.ShowHelp(this, this.helpFile, this.topic);
      }
    }

    private void spOpt_View_ButtonDrawMode_Always_CheckedChanged(object sender, EventArgs e)
    {
      if (this.spOpt_View_ButtonDrawMode_Always.Checked)
      {
        this.spOpt_View_ButtonDrawMode_AlwaysPrimary.Checked = false;
        this.spOpt_View_ButtonDrawMode_AlwaysSecondary.Checked = false;
        this.spOpt_View_ButtonDrawMode_AlwaysEditButton.Checked = false;
        this.spOpt_View_ButtonDrawMode_CurrentRow.Checked = false;
        this.spOpt_View_ButtonDrawMode_CurrentColumn.Checked = false;
        this.spOpt_View_ButtonDrawMode_CurrentCell.Checked = false;
      }
    }

    private void spOpt_View_ButtonDrawMode_EditMode_CheckedChanged(object sender, EventArgs e)
    {
      if (((CheckBox)sender).Checked)
      {
        this.spOpt_View_ButtonDrawMode_Always.Checked = false;
      }
    }

    private void spOpt_View_ButtonDrawMode_Others_CheckedChanged(object sender, EventArgs e)
    {
      if (((CheckBox)sender).Checked)
      {
        this.spOpt_View_ButtonDrawMode_Always.Checked = false;
      }
    }

    private void SpreadOptions_Load(object sender, EventArgs e)
    {
      if (common.rm != null)
      {
        this.Text = common.rm.GetString("SpreadOpt_Title");
        this.spOpt_TabEdit.Text = common.rm.GetString("spOpt_TabEdit");
        this.spOpt_TabScrollBars.Text = common.rm.GetString("spOpt_TabScrollBars");
        this.spOpt_TabGeneral.Text = common.rm.GetString("spOpt_TabGeneral");
        this.spOpt_TabSplitBox.Text = common.rm.GetString("spOpt_TabSplitBox");
        this.spOpt_TabView.Text = common.rm.GetString("spOpt_TabView");
        this.spOpt_Edit_MoveActiveOnFocus.Text = common.rm.GetString("spOpt_Edit_MoveActiveOnFocus");
        this.spOpt_Edit_EditModeReplace.Text = common.rm.GetString("spOpt_Edit_EditModeReplace");
        this.spOpt_Edit_EditModePermanent.Text = common.rm.GetString("spOpt_Edit_EditModePermanent");
        this.spOpt_Edit_AllowEditOverflow.Text = common.rm.GetString("spOpt_Edit_AllowEditOverflow");
        this.spOpt_EditSettings.Text = common.rm.GetString("spOpt_EditSettings");
        this.spOpt_SB_VSB_Always.Text = common.rm.GetString("spOpt_SB_VSB_Always");
        this.spOpt_SB_VSB_Never.Text = common.rm.GetString("spOpt_SB_VSB_Never");
        this.spOpt_SB_VSB_AsNeeded.Text = common.rm.GetString("spOpt_SB_VSB_AsNeeded");
        this.spOpt_SB_VertScrollBar.Text = common.rm.GetString("spOpt_SB_VertScrollBar");
        this.spOpt_SB_HSB_Always.Text = common.rm.GetString("spOpt_SB_HSB_Always");
        this.spOpt_SB_HSB_Never.Text = common.rm.GetString("spOpt_SB_HSB_Never");
        this.spOpt_SB_HSB_AsNeeded.Text = common.rm.GetString("spOpt_SB_HSB_AsNeeded");
        this.spOpt_SB_HorzScrollBar.Text = common.rm.GetString("spOpt_SB_HorzScrollBar");
        this.spOpt_SB_ScrollTip_Horizontal.Text = common.rm.GetString("spOpt_SB_ScrollTip_Horizontal");
        this.spOpt_SB_ScrollTip_Vertical.Text = common.rm.GetString("spOpt_SB_ScrollTip_Vertical");
        this.spOpt_SB_ScrollTip.Text = common.rm.GetString("spOpt_SB_ScrollTip");
        this.spOpt_SB_SBTrack_Horizontal.Text = common.rm.GetString("spOpt_SB_SBTrack_Horizontal");
        this.spOpt_SB_SBTrack_Vertical.Text = common.rm.GetString("spOpt_SB_SBTrack_Vertical");
        this.spOpt_SB_Tracking.Text = common.rm.GetString("spOpt_SB_Tracking");
        this.spOpt_SB_Settings.Text = common.rm.GetString("spOpt_SB_Settings");
        this.spOpt_SB_ScrollBarShowMax.Text = common.rm.GetString("spOpt_SB_ScrollBarShowMax");
        this.spOpt_SB_ScrollBarMaxAlign.Text = common.rm.GetString("spOpt_SB_ScrollBarMaxAlign");
        this.spOpt_Gen_RowHeaders.Text = common.rm.GetString("spOpt_Gen_RowHeaders");
        this.spOpt_Gen_NoHeaders.Text = common.rm.GetString("spOpt_Gen_NoHeaders");
        this.spOpt_Gen_ColumnHeaders.Text = common.rm.GetString("spOpt_Gen_ColumnHeaders");
        this.spOpt_Gen_ClipOptions.Text = common.rm.GetString("spOpt_Gen_ClipOptions");
        this.spOpt_Gen_AllHeaders.Text = common.rm.GetString("spOpt_Gen_AllHeaders");
        this.spOpt_Gen_TabStripRatioLabel.Text = common.rm.GetString("spOpt_Gen_TabStripRatioLabel");
        this.spOpt_Gen_TabStrip_AsNeeded.Text = common.rm.GetString("spOpt_Gen_TabStrip_AsNeeded");
        this.spOpt_Gen_TabStrip_Never.Text = common.rm.GetString("spOpt_Gen_TabStrip_Never");
        this.spOpt_Gen_TabStrip_Always.Text = common.rm.GetString("spOpt_Gen_TabStrip_Always");
        this.spOpt_Gen_TabStrip.Text = common.rm.GetString("spOpt_Gen_TabStrip");
        this.spOpt_Gen_Settings.Text = common.rm.GetString("spOpt_Gen_Settings");
        this.spOpt_Gen_AutoClipboard.Text = common.rm.GetString("spOpt_Gen_AutoClipboard");
        this.spOpt_Gen_AllowUserFormulas.Text = common.rm.GetString("spOpt_Gen_AllowUserFormulas");
        this.spOpt_Gen_AllowDragFill.Text = common.rm.GetString("spOpt_Gen_AllowDragFill");
        this.spOpt_Gen_AllowDragDrop.Text = common.rm.GetString("spOpt_Gen_AllowDragDrop");
        this.spOpt_SplitBox_RowDisplay.Text = common.rm.GetString("spOpt_SplitBox_RowDisplay");
        this.spOpt_SplitBox_Row_Always.Text = common.rm.GetString("spOpt_SplitBox_Row_Always");
        this.spOpt_SplitBox_Row_Never.Text = common.rm.GetString("spOpt_SplitBox_Row_Never");
        this.spOpt_SplitBox_Row_AsNeeded.Text = common.rm.GetString("spOpt_SplitBox_Row_AsNeeded");
        this.spOpt_SplitBox_Row_Trailing.Text = common.rm.GetString("spOpt_SplitBox_Row_Trailing");
        this.spOpt_SplitBox_RowAlign.Text = common.rm.GetString("spOpt_SplitBox_RowAlign");
        this.spOpt_SplitBox_Row_Leading.Text = common.rm.GetString("spOpt_SplitBox_Row_Leading");
        this.spOpt_SplitBox_ColDisplay.Text = common.rm.GetString("spOpt_SplitBox_ColDisplay");
        this.spOpt_SplitBox_Col_Always.Text = common.rm.GetString("spOpt_SplitBox_Col_Always");
        this.spOpt_SplitBox_Col_Never.Text = common.rm.GetString("spOpt_SplitBox_Col_Never");
        this.spOpt_SplitBox_Col_AsNeeded.Text = common.rm.GetString("spOpt_SplitBox_Col_AsNeeded");
        this.spOpt_SplitBox_ColAlign.Text = common.rm.GetString("spOpt_SplitBox_ColAlign");
        this.spOpt_SplitBox_Col_Leading.Text = common.rm.GetString("spOpt_SplitBox_Col_Leading");
        this.spOpt_SplitBox_Col_Trailing.Text = common.rm.GetString("spOpt_SplitBox_Col_Trailing");
        this.spOpt_View_GroupBox1.Text = common.rm.GetString("spOpt_View_GroupBox1");
        this.spOpt_View_SelectBlockOptions_Sheet.Text = common.rm.GetString("spOpt_View_SelectBlockOptions_Sheet");
        this.spOpt_View_SelectBlockOptions_Columns.Text = common.rm.GetString("spOpt_View_SelectBlockOptions_Columns");
        this.spOpt_View_SelectBlockOptions_Rows.Text = common.rm.GetString("spOpt_View_SelectBlockOptions_Rows");
        this.spOpt_View_SelectBlockOptions_Cells.Text = common.rm.GetString("spOpt_View_SelectBlockOptions_Cells");
        this.spOpt_View_ButtonDrawMode_AlwaysSecondary.Text = common.rm.GetString("spOpt_View_ButtonDrawMode_AlwaysSecondary");
        this.spOpt_View_ButtonDrawMode_AlwaysPrimary.Text = common.rm.GetString("spOpt_View_ButtonDrawMode_AlwaysPrimary");
        this.spOpt_View_ButtonDrawMode_CurrentRow.Text = common.rm.GetString("spOpt_View_ButtonDrawMode_CurrentRow");
        this.spOpt_View_ButtonDrawMode_CurrentColumn.Text = common.rm.GetString("spOpt_View_ButtonDrawMode_CurrentColumn");
        this.spOpt_View_ButtonDrawMode_CurrentCell.Text = common.rm.GetString("spOpt_View_ButtonDrawMode_CurrentCell");
        this.spOpt_View_ButtonDrawMode_Always.Text = common.rm.GetString("spOpt_View_ButtonDrawMode_Always");
        this.spOpt_View_BlockMode.Text = common.rm.GetString("spOpt_View_BlockMode");
        this.spOpt_View_CellButtons.Text = common.rm.GetString("spOpt_View_CellButtons");
        this.spOpt_View_RetainSelectionBlock.Text = common.rm.GetString("spOpt_View_RetainSelectionBlock");
        this.spOpt_View_CellNoteIndicatorVisible.Text = common.rm.GetString("spOpt_View_CellNoteIndicatorVisible");
        this.spOpt_View_AllowCellOverflow.Text = common.rm.GetString("spOpt_View_AllowCellOverflow");
        this.spOpt_View_Settings.Text = common.rm.GetString("spOpt_View_Settings");
        this.spOpt_Cancel.Text = common.rm.GetString("spOpt_Cancel");
        this.spOpt_OK.Text = common.rm.GetString("spOpt_OK");
        this.spOpt_Help.Text = common.rm.GetString("spOpt_Help");
      }
      this.spOpt_Gen_TabStripRatioLabel.Visible = false;
      this.spOpt_Gen_TabStrip_AsNeeded.Visible = false;
      this.spOpt_Gen_TabStrip_Never.Visible = false;
      this.spOpt_Gen_TabStrip_Always.Visible = false;
      this.spOpt_Gen_TabStrip.Visible = false;
      this.spOpt_Gen_TabStripRatio.Visible = false;
      this.spOpt_TabTitle.Text = common.rm.GetString("spOpt_TabTitle");
      this.gbSubtitle.Text = common.rm.GetString("spOpt_TabTitle_Subtitle");
      this.label3.Text = common.rm.GetString("spOpt_TabTitle_Label_Subtitle");
      this.chbShowSubtitle.Text = common.rm.GetString("spOpt_TabTitle_ShowSubtitle");
      this.gbTitle.Text = common.rm.GetString("spOpt_TabTitle_Title");
      this.label2.Text = common.rm.GetString("spOpt_TabTitle_Label_Title");
      this.chbShowTitle.Text = common.rm.GetString("spOpt_TabTitle_ShowTitle");
      this.spOpt_View_ButtonDrawMode_AlwaysEditButton.Text = common.rm.GetString("spOpt_View_ButtonDrawMode_AlwaysEditButton");
      this.FormExchange(true);
      if (this.Owner != null)
      {
        this.Icon = this.Owner.Icon;
      }
    }

    private void SpreadOptionsDlg_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.F1)
      {
        e.Handled = true;
        this.spOpt_Help_Click(sender, EventArgs.Empty);
      }
    }

    private struct EnumButtonList
    {
      public object button;
      public int buttonVal;
    }
  }
}
