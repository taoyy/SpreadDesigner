﻿using System.Drawing;
using System.Windows.Forms;
namespace SpreadDesigner
{
  partial class TextOptionsDlg
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.TextOptionsLabel1 = new Label();
      this.CellDelim = new TextBox();
      this.ColumnDelim = new TextBox();
      this.TextOptionsLabel2 = new Label();
      this.RowDelim = new TextBox();
      this.TextOptionsLabel3 = new Label();
      this.TextOptionsGroupBox1 = new GroupBox();
      this.RowApply = new Button();
      this.ColumnApply = new Button();
      this.CellApply = new Button();
      this.SpecialChars = new ListBox();
      this.TextOptionsGroupBox2 = new GroupBox();
      this.IncludeRowHdr = new CheckBox();
      this.IncludeColumnHdr = new CheckBox();
      this.TextOptionsOKBtn = new Button();
      this.TextOptionsCancelBtn = new Button();
      this.TextOptionsFormatted = new CheckBox();
      this.TextOptionsNoteArea = new Label();
      this.TextOptionsGroupBox1.SuspendLayout();
      this.TextOptionsGroupBox2.SuspendLayout();
      this.SuspendLayout();
      this.TextOptionsLabel1.ImeMode = ImeMode.NoControl;
      Point point2 = new Point(11, 0x5f);
      this.TextOptionsLabel1.Location = point2;
      this.TextOptionsLabel1.Name = "TextOptionsLabel1";
      Size size2 = new Size(0x90, 0x17);
      this.TextOptionsLabel1.Size = size2;
      this.TextOptionsLabel1.TabIndex = 5;
      this.TextOptionsLabel1.Text = "C&ell Delimiter";
      this.TextOptionsLabel1.TextAlign = ContentAlignment.MiddleLeft;
      point2 = new Point(0xad, 0x5f);
      this.CellDelim.Location = point2;
      this.CellDelim.Name = "CellDelim";
      size2 = new Size(0x56, 20);
      this.CellDelim.Size = size2;
      this.CellDelim.TabIndex = 6;
      this.CellDelim.Text = "";
      point2 = new Point(0xad, 0x16);
      this.ColumnDelim.Location = point2;
      this.ColumnDelim.Name = "ColumnDelim";
      size2 = new Size(0x56, 20);
      this.ColumnDelim.Size = size2;
      this.ColumnDelim.TabIndex = 2;
      this.ColumnDelim.Text = "";
      this.TextOptionsLabel2.ImeMode = ImeMode.NoControl;
      point2 = new Point(11, 0x16);
      this.TextOptionsLabel2.Location = point2;
      this.TextOptionsLabel2.Name = "TextOptionsLabel2";
      size2 = new Size(0x90, 0x17);
      this.TextOptionsLabel2.Size = size2;
      this.TextOptionsLabel2.TabIndex = 1;
      this.TextOptionsLabel2.Text = "C&olumn Delimiter";
      this.TextOptionsLabel2.TextAlign = ContentAlignment.MiddleLeft;
      point2 = new Point(0xad, 0x3b);
      this.RowDelim.Location = point2;
      this.RowDelim.Name = "RowDelim";
      size2 = new Size(0x56, 20);
      this.RowDelim.Size = size2;
      this.RowDelim.TabIndex = 4;
      this.RowDelim.Text = "";
      this.TextOptionsLabel3.ImeMode = ImeMode.NoControl;
      point2 = new Point(11, 0x3b);
      this.TextOptionsLabel3.Location = point2;
      this.TextOptionsLabel3.Name = "TextOptionsLabel3";
      size2 = new Size(0x90, 0x16);
      this.TextOptionsLabel3.Size = size2;
      this.TextOptionsLabel3.TabIndex = 3;
      this.TextOptionsLabel3.Text = "Ro&w Delimiter";
      this.TextOptionsLabel3.TextAlign = ContentAlignment.MiddleLeft;
      this.TextOptionsGroupBox1.BackColor = Color.WhiteSmoke;
      this.TextOptionsGroupBox1.Controls.Add(this.RowApply);
      this.TextOptionsGroupBox1.Controls.Add(this.ColumnApply);
      this.TextOptionsGroupBox1.Controls.Add(this.CellApply);
      this.TextOptionsGroupBox1.Controls.Add(this.SpecialChars);
      point2 = new Point(0x10d, 9);
      this.TextOptionsGroupBox1.Location = point2;
      this.TextOptionsGroupBox1.Name = "TextOptionsGroupBox1";
      size2 = new Size(0x11e, 0x83);
      this.TextOptionsGroupBox1.Size = size2;
      this.TextOptionsGroupBox1.TabIndex = 7;
      this.TextOptionsGroupBox1.TabStop = false;
      this.TextOptionsGroupBox1.Text = "&Special Delimiters";
      this.RowApply.ImeMode = ImeMode.NoControl;
      point2 = new Point(0xbd, 0x33);
      this.RowApply.Location = point2;
      this.RowApply.Name = "RowApply";
      size2 = new Size(0x5f, 0x18);
      this.RowApply.Size = size2;
      this.RowApply.TabIndex = 10;
      this.RowApply.Tag = "Row";
      this.RowApply.Text = "App&ly to Row";
      this.RowApply.Click += this.Apply_Click;
      this.ColumnApply.ImeMode = ImeMode.NoControl;
      point2 = new Point(0xbd, 0x15);
      this.ColumnApply.Location = point2;
      this.ColumnApply.Name = "ColumnApply";
      size2 = new Size(0x5f, 0x18);
      this.ColumnApply.Size = size2;
      this.ColumnApply.TabIndex = 9;
      this.ColumnApply.Tag = "Column";
      this.ColumnApply.Text = "Ap&ply to Column";
      this.ColumnApply.Click += this.Apply_Click;

      this.CellApply.ImeMode = ImeMode.NoControl;
      point2 = new Point(0xbd, 0x51);
      this.CellApply.Location = point2;
      this.CellApply.Name = "CellApply";
      size2 = new Size(0x5f, 0x18);
      this.CellApply.Size = size2;
      this.CellApply.TabIndex = 11;
      this.CellApply.Tag = "Cell";
      this.CellApply.Text = "Appl&y to Cell";
      this.CellApply.Click += this.Apply_Click;

      this.SpecialChars.HorizontalScrollbar = true;
      this.SpecialChars.Items.AddRange(new object[] { "Default", "Tab <tab>", "Carriage return <cr>", "Line feed <lf>", "Carriage return/Line Feed <cr><lf>" });
      point2 = new Point(11, 0x16);
      this.SpecialChars.Location = point2;
      this.SpecialChars.Name = "SpecialChars";
      size2 = new Size(0xb1, 0x52);
      this.SpecialChars.Size = size2;
      this.SpecialChars.TabIndex = 8;
      this.TextOptionsGroupBox2.BackColor = Color.WhiteSmoke;
      this.TextOptionsGroupBox2.Controls.Add(this.IncludeRowHdr);
      this.TextOptionsGroupBox2.Controls.Add(this.IncludeColumnHdr);
      point2 = new Point(13, 0x88);
      this.TextOptionsGroupBox2.Location = point2;
      this.TextOptionsGroupBox2.Name = "TextOptionsGroupBox2";
      size2 = new Size(0xfb, 90);
      this.TextOptionsGroupBox2.Size = size2;
      this.TextOptionsGroupBox2.TabIndex = 12;
      this.TextOptionsGroupBox2.TabStop = false;
      this.TextOptionsGroupBox2.Text = "&Include Headers";
      this.IncludeRowHdr.ImeMode = ImeMode.NoControl;
      point2 = new Point(13, 0x38);
      this.IncludeRowHdr.Location = point2;
      this.IncludeRowHdr.Name = "IncludeRowHdr";
      size2 = new Size(0xa3, 0x17);
      this.IncludeRowHdr.Size = size2;
      this.IncludeRowHdr.TabIndex = 14;
      this.IncludeRowHdr.Text = "&Row";
      this.IncludeColumnHdr.ImeMode = ImeMode.NoControl;
      point2 = new Point(13, 0x1c);
      this.IncludeColumnHdr.Location = point2;
      this.IncludeColumnHdr.Name = "IncludeColumnHdr";
      size2 = new Size(0xa2, 0x16);
      this.IncludeColumnHdr.Size = size2;
      this.IncludeColumnHdr.TabIndex = 13;
      this.IncludeColumnHdr.Text = "&Column";
      this.TextOptionsOKBtn.ImeMode = ImeMode.NoControl;
      point2 = new Point(0x17b, 0xfe);
      this.TextOptionsOKBtn.Location = point2;
      this.TextOptionsOKBtn.Name = "TextOptionsOKBtn";
      size2 = new Size(80, 0x18);
      this.TextOptionsOKBtn.Size = size2;
      this.TextOptionsOKBtn.TabIndex = 0x10;
      this.TextOptionsOKBtn.Text = "OK";
      this.TextOptionsOKBtn.Click += this.TextOptionsOKBtn_Click;

      this.TextOptionsCancelBtn.DialogResult = DialogResult.Cancel;
      this.TextOptionsCancelBtn.ImeMode = ImeMode.NoControl;
      point2 = new Point(0x1d1, 0xfe);
      this.TextOptionsCancelBtn.Location = point2;
      this.TextOptionsCancelBtn.Name = "TextOptionsCancelBtn";
      size2 = new Size(80, 0x18);
      this.TextOptionsCancelBtn.Size = size2;
      this.TextOptionsCancelBtn.TabIndex = 0x11;
      this.TextOptionsCancelBtn.Text = "Cancel";
      this.TextOptionsCancelBtn.Click += this.TextOptionsCancelBtn_Click;

      this.TextOptionsFormatted.ImeMode = ImeMode.NoControl;
      point2 = new Point(280, 0x92);
      this.TextOptionsFormatted.Location = point2;
      this.TextOptionsFormatted.Name = "TextOptionsFormatted";
      size2 = new Size(0x110, 0x16);
      this.TextOptionsFormatted.Size = size2;
      this.TextOptionsFormatted.TabIndex = 15;
      this.TextOptionsFormatted.Text = "&Data contains formatting";
      this.TextOptionsNoteArea.FlatStyle = FlatStyle.Flat;
      this.TextOptionsNoteArea.ImeMode = ImeMode.NoControl;
      point2 = new Point(1, 0xe9);
      this.TextOptionsNoteArea.Location = point2;
      this.TextOptionsNoteArea.Name = "TextOptionsNoteArea";
      size2 = new Size(320, 0x2d);
      this.TextOptionsNoteArea.Size = size2;
      this.TextOptionsNoteArea.TabIndex = 0x12;
      this.AcceptButton = this.TextOptionsOKBtn;
      size2 = new Size(5, 13);
      this.AutoScaleBaseSize = size2;
      this.BackColor = Color.WhiteSmoke;
      this.CancelButton = this.TextOptionsCancelBtn;
      size2 = new Size(0x22d, 0x11a);
      this.ClientSize = size2;
      this.Controls.AddRange(new Control[] { this.TextOptionsNoteArea, this.TextOptionsFormatted, this.TextOptionsOKBtn, this.TextOptionsCancelBtn, this.TextOptionsGroupBox2, this.TextOptionsGroupBox1, this.RowDelim, this.TextOptionsLabel3, this.ColumnDelim, this.TextOptionsLabel2, this.CellDelim, this.TextOptionsLabel1 });
      this.FormBorderStyle = FormBorderStyle.FixedDialog;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "TextOptionsDlg";
      this.StartPosition = FormStartPosition.CenterScreen;
      this.Text = "Custom File Options";
      this.TextOptionsGroupBox1.ResumeLayout(false);
      this.TextOptionsGroupBox2.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();
    }


    public Button CellApply;
    public TextBox CellDelim;
    public Button ColumnApply;
    public TextBox ColumnDelim;
    public CheckBox IncludeColumnHdr;
    public CheckBox IncludeRowHdr;
    public Button RowApply;
    public TextBox RowDelim;
    public ListBox SpecialChars;
    public Button TextOptionsCancelBtn;
    public CheckBox TextOptionsFormatted;
    public GroupBox TextOptionsGroupBox1;
    public GroupBox TextOptionsGroupBox2;
    public Label TextOptionsLabel1;
    public Label TextOptionsLabel2;
    public Label TextOptionsLabel3;
    public Label TextOptionsNoteArea;
    public Button TextOptionsOKBtn;
    private string cellDefault;
    private string colDefault;
    private string rowDefault;
    #endregion
  }
}