﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpreadDesigner
{
  public class SortDlg : System.Windows.Forms.Form
  {
    System.Resources.ResourceSet rm = null;
    // Variable representing the spreadsheet being designed

    private FarPoint.Win.Spread.FpSpread SpreadWrapper1;
    public SortDlg(FarPoint.Win.Spread.FpSpread spread)
      : base()
    {

      if (FarPoint.Win.Spread.Design.common.rm != null)
      {
        rm = FarPoint.Win.Spread.Design.common.rm;
      }
      else
      {
        FarPoint.Win.Spread.Design.common.LoadResourceManagerForStrings();
        rm = FarPoint.Win.Spread.Design.common.rm;
      }

      KeyDown += SortDlg_KeyDown;
      Load += SortDlg_Load;
      // Set the internal variable to contain the spreadsheet being designed
      this.SpreadWrapper1 = spread;
      //This call is required by the Windows Form Designer.
      InitializeComponent();

    }

    #region " Windows Form Designer generated code "

    public SortDlg()
      : base()
    {

      if (FarPoint.Win.Spread.Design.common.rm != null)
      {
        rm = FarPoint.Win.Spread.Design.common.rm;
      }
      else
      {
        FarPoint.Win.Spread.Design.common.LoadResourceManagerForStrings();
        rm = FarPoint.Win.Spread.Design.common.rm;
      }
      KeyDown += SortDlg_KeyDown;
      Load += SortDlg_Load;

      //This call is required by the Windows Form Designer.
      InitializeComponent();

      //Add any initialization after the InitializeComponent() call

    }

    //Form overrides dispose to clean up the component list.
    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        if ((components != null))
        {
          components.Dispose();
        }
      }
      base.Dispose(disposing);
    }

    //Required by the Windows Form Designer

    private System.ComponentModel.IContainer components;
    //NOTE: The following procedure is required by the Windows Form Designer
    //It can be modified using the Windows Form Designer.  
    //Do not modify it using the code editor.
    private System.Windows.Forms.Label withEventsField_SortLabel1;
    protected internal System.Windows.Forms.Label SortLabel1
    {
      get { return withEventsField_SortLabel1; }
      set
      {
        if (withEventsField_SortLabel1 != null)
        {
          withEventsField_SortLabel1.Paint -= Label_Paint;
        }
        withEventsField_SortLabel1 = value;
        if (withEventsField_SortLabel1 != null)
        {
          withEventsField_SortLabel1.Paint += Label_Paint;
        }
      }
    }
    private System.Windows.Forms.RadioButton withEventsField_SortByRows;
    protected internal System.Windows.Forms.RadioButton SortByRows
    {
      get { return withEventsField_SortByRows; }
      set
      {
        if (withEventsField_SortByRows != null)
        {
          withEventsField_SortByRows.CheckedChanged -= SortByRows_CheckedChanged;
        }
        withEventsField_SortByRows = value;
        if (withEventsField_SortByRows != null)
        {
          withEventsField_SortByRows.CheckedChanged += SortByRows_CheckedChanged;
        }
      }
    }
    protected internal System.Windows.Forms.Panel Panel1;
    private System.Windows.Forms.RadioButton withEventsField_SortByColumns;
    protected internal System.Windows.Forms.RadioButton SortByColumns
    {
      get { return withEventsField_SortByColumns; }
      set
      {
        if (withEventsField_SortByColumns != null)
        {
          withEventsField_SortByColumns.CheckedChanged -= SortByColumns_CheckedChanged;
        }
        withEventsField_SortByColumns = value;
        if (withEventsField_SortByColumns != null)
        {
          withEventsField_SortByColumns.CheckedChanged += SortByColumns_CheckedChanged;
        }
      }
    }
    private System.Windows.Forms.Label withEventsField_SortLabel2;
    protected internal System.Windows.Forms.Label SortLabel2
    {
      get { return withEventsField_SortLabel2; }
      set
      {
        if (withEventsField_SortLabel2 != null)
        {
          withEventsField_SortLabel2.Paint -= Label_Paint;
        }
        withEventsField_SortLabel2 = value;
        if (withEventsField_SortLabel2 != null)
        {
          withEventsField_SortLabel2.Paint += Label_Paint;
        }
      }
    }
    protected internal System.Windows.Forms.Panel Panel2;
    protected internal System.Windows.Forms.ComboBox Key1;
    protected internal System.Windows.Forms.RadioButton Key1Asc;
    protected internal System.Windows.Forms.RadioButton Key1Des;
    protected internal System.Windows.Forms.Panel Panel3;
    protected internal System.Windows.Forms.RadioButton Key2Des;
    protected internal System.Windows.Forms.RadioButton Key2Asc;
    protected internal System.Windows.Forms.ComboBox Key2;
    private System.Windows.Forms.Label withEventsField_SortLabel3;
    protected internal System.Windows.Forms.Label SortLabel3
    {
      get { return withEventsField_SortLabel3; }
      set
      {
        if (withEventsField_SortLabel3 != null)
        {
          withEventsField_SortLabel3.Paint -= Label_Paint;
        }
        withEventsField_SortLabel3 = value;
        if (withEventsField_SortLabel3 != null)
        {
          withEventsField_SortLabel3.Paint += Label_Paint;
        }
      }
    }
    protected internal System.Windows.Forms.Panel Panel4;
    private System.Windows.Forms.Label withEventsField_SortLabel4;
    protected internal System.Windows.Forms.Label SortLabel4
    {
      get { return withEventsField_SortLabel4; }
      set
      {
        if (withEventsField_SortLabel4 != null)
        {
          withEventsField_SortLabel4.Paint -= Label_Paint;
        }
        withEventsField_SortLabel4 = value;
        if (withEventsField_SortLabel4 != null)
        {
          withEventsField_SortLabel4.Paint += Label_Paint;
        }
      }
    }
    protected internal System.Windows.Forms.RadioButton Key3Des;
    protected internal System.Windows.Forms.RadioButton Key3Asc;
    protected internal System.Windows.Forms.ComboBox Key3;
    private System.Windows.Forms.Button withEventsField_SortOK;
    protected internal System.Windows.Forms.Button SortOK
    {
      get { return withEventsField_SortOK; }
      set
      {
        if (withEventsField_SortOK != null)
        {
          withEventsField_SortOK.Click -= OK_Click;
        }
        withEventsField_SortOK = value;
        if (withEventsField_SortOK != null)
        {
          withEventsField_SortOK.Click += OK_Click;
        }
      }
    }
    protected internal System.Windows.Forms.Button SortCancel;
    private System.Windows.Forms.Label withEventsField_SortLabel5;
    protected internal System.Windows.Forms.Label SortLabel5
    {
      get { return withEventsField_SortLabel5; }
      set
      {
        if (withEventsField_SortLabel5 != null)
        {
          withEventsField_SortLabel5.Paint -= Label_Paint;
        }
        withEventsField_SortLabel5 = value;
        if (withEventsField_SortLabel5 != null)
        {
          withEventsField_SortLabel5.Paint += Label_Paint;
        }
      }
    }
    protected internal System.Windows.Forms.CheckBox MoveEntire;
    [System.Diagnostics.DebuggerStepThrough()]
    private void InitializeComponent()
    {
      this.SortLabel1 = new System.Windows.Forms.Label();
      this.SortByRows = new System.Windows.Forms.RadioButton();
      this.Panel1 = new System.Windows.Forms.Panel();
      this.SortByColumns = new System.Windows.Forms.RadioButton();
      this.SortLabel2 = new System.Windows.Forms.Label();
      this.Panel2 = new System.Windows.Forms.Panel();
      this.Key1Des = new System.Windows.Forms.RadioButton();
      this.Key1Asc = new System.Windows.Forms.RadioButton();
      this.Key1 = new System.Windows.Forms.ComboBox();
      this.Panel3 = new System.Windows.Forms.Panel();
      this.Key2Des = new System.Windows.Forms.RadioButton();
      this.Key2Asc = new System.Windows.Forms.RadioButton();
      this.Key2 = new System.Windows.Forms.ComboBox();
      this.SortLabel3 = new System.Windows.Forms.Label();
      this.Panel4 = new System.Windows.Forms.Panel();
      this.Key3Des = new System.Windows.Forms.RadioButton();
      this.Key3Asc = new System.Windows.Forms.RadioButton();
      this.Key3 = new System.Windows.Forms.ComboBox();
      this.SortLabel4 = new System.Windows.Forms.Label();
      this.SortOK = new System.Windows.Forms.Button();
      this.SortCancel = new System.Windows.Forms.Button();
      this.SortLabel5 = new System.Windows.Forms.Label();
      this.MoveEntire = new System.Windows.Forms.CheckBox();
      this.Panel1.SuspendLayout();
      this.Panel2.SuspendLayout();
      this.Panel3.SuspendLayout();
      this.Panel4.SuspendLayout();
      this.SuspendLayout();
      //
      //SortLabel1
      //
      this.SortLabel1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.SortLabel1.Location = new System.Drawing.Point(8, 8);
      this.SortLabel1.Name = "SortLabel1";
      this.SortLabel1.Size = new System.Drawing.Size(288, 16);
      this.SortLabel1.TabIndex = 1;
      this.SortLabel1.Text = "&Sort by:";
      //
      //SortByRows
      //
      this.SortByRows.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.SortByRows.Location = new System.Drawing.Point(16, 32);
      this.SortByRows.Name = "SortByRows";
      this.SortByRows.Size = new System.Drawing.Size(104, 16);
      this.SortByRows.TabIndex = 2;
      this.SortByRows.Text = "&Rows";
      //
      //Panel1
      //
      this.Panel1.Controls.Add(this.SortByColumns);
      this.Panel1.Controls.Add(this.SortLabel1);
      this.Panel1.Controls.Add(this.SortByRows);
      this.Panel1.Location = new System.Drawing.Point(0, 8);
      this.Panel1.Name = "Panel1";
      this.Panel1.Size = new System.Drawing.Size(304, 64);
      this.Panel1.TabIndex = 0;
      //
      //SortByColumns
      //
      this.SortByColumns.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.SortByColumns.Location = new System.Drawing.Point(128, 32);
      this.SortByColumns.Name = "SortByColumns";
      this.SortByColumns.Size = new System.Drawing.Size(104, 16);
      this.SortByColumns.TabIndex = 3;
      this.SortByColumns.Text = "&Columns";
      //
      //SortLabel2
      //
      this.SortLabel2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.SortLabel2.Location = new System.Drawing.Point(8, 8);
      this.SortLabel2.Name = "SortLabel2";
      this.SortLabel2.Size = new System.Drawing.Size(288, 16);
      this.SortLabel2.TabIndex = 5;
      this.SortLabel2.Text = "&1st Key:";
      //
      //Panel2
      //
      this.Panel2.Controls.Add(this.Key1Des);
      this.Panel2.Controls.Add(this.Key1Asc);
      this.Panel2.Controls.Add(this.Key1);
      this.Panel2.Controls.Add(this.SortLabel2);
      this.Panel2.Location = new System.Drawing.Point(0, 56);
      this.Panel2.Name = "Panel2";
      this.Panel2.Size = new System.Drawing.Size(304, 66);
      this.Panel2.TabIndex = 4;
      //
      //Key1Des
      //
      this.Key1Des.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.Key1Des.Location = new System.Drawing.Point(187, 46);
      this.Key1Des.Name = "Key1Des";
      this.Key1Des.Size = new System.Drawing.Size(104, 18);
      this.Key1Des.TabIndex = 8;
      this.Key1Des.Text = "&Descending";
      //
      //Key1Asc
      //
      this.Key1Asc.Checked = true;
      this.Key1Asc.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.Key1Asc.Location = new System.Drawing.Point(187, 27);
      this.Key1Asc.Name = "Key1Asc";
      this.Key1Asc.Size = new System.Drawing.Size(104, 17);
      this.Key1Asc.TabIndex = 7;
      this.Key1Asc.TabStop = true;
      this.Key1Asc.Text = "&Ascending";
      //
      //Key1
      //
      this.Key1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.Key1.ItemHeight = 13;
      this.Key1.Location = new System.Drawing.Point(16, 32);
      this.Key1.Name = "Key1";
      this.Key1.Size = new System.Drawing.Size(160, 21);
      this.Key1.TabIndex = 6;
      //
      //Panel3
      //
      this.Panel3.Controls.Add(this.Key2Des);
      this.Panel3.Controls.Add(this.Key2Asc);
      this.Panel3.Controls.Add(this.Key2);
      this.Panel3.Controls.Add(this.SortLabel3);
      this.Panel3.Location = new System.Drawing.Point(0, 120);
      this.Panel3.Name = "Panel3";
      this.Panel3.Size = new System.Drawing.Size(304, 68);
      this.Panel3.TabIndex = 9;
      //
      //Key2Des
      //
      this.Key2Des.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.Key2Des.Location = new System.Drawing.Point(187, 46);
      this.Key2Des.Name = "Key2Des";
      this.Key2Des.Size = new System.Drawing.Size(104, 18);
      this.Key2Des.TabIndex = 13;
      this.Key2Des.Text = "D&escending";
      //
      //Key2Asc
      //
      this.Key2Asc.Checked = true;
      this.Key2Asc.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.Key2Asc.Location = new System.Drawing.Point(187, 27);
      this.Key2Asc.Name = "Key2Asc";
      this.Key2Asc.Size = new System.Drawing.Size(104, 17);
      this.Key2Asc.TabIndex = 12;
      this.Key2Asc.TabStop = true;
      this.Key2Asc.Text = "Asce&nding";
      //
      //Key2
      //
      this.Key2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.Key2.ItemHeight = 13;
      this.Key2.Location = new System.Drawing.Point(16, 32);
      this.Key2.Name = "Key2";
      this.Key2.Size = new System.Drawing.Size(160, 21);
      this.Key2.TabIndex = 11;
      //
      //SortLabel3
      //
      this.SortLabel3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.SortLabel3.Location = new System.Drawing.Point(8, 8);
      this.SortLabel3.Name = "SortLabel3";
      this.SortLabel3.Size = new System.Drawing.Size(288, 16);
      this.SortLabel3.TabIndex = 10;
      this.SortLabel3.Text = "&2nd Key:";
      //
      //Panel4
      //
      this.Panel4.Controls.Add(this.Key3Des);
      this.Panel4.Controls.Add(this.Key3Asc);
      this.Panel4.Controls.Add(this.Key3);
      this.Panel4.Controls.Add(this.SortLabel4);
      this.Panel4.Location = new System.Drawing.Point(1, 190);
      this.Panel4.Name = "Panel4";
      this.Panel4.Size = new System.Drawing.Size(304, 72);
      this.Panel4.TabIndex = 14;
      //
      //Key3Des
      //
      this.Key3Des.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.Key3Des.Location = new System.Drawing.Point(187, 46);
      this.Key3Des.Name = "Key3Des";
      this.Key3Des.Size = new System.Drawing.Size(104, 19);
      this.Key3Des.TabIndex = 18;
      this.Key3Des.Text = "Descend&ing";
      //
      //Key3Asc
      //
      this.Key3Asc.Checked = true;
      this.Key3Asc.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.Key3Asc.Location = new System.Drawing.Point(187, 27);
      this.Key3Asc.Name = "Key3Asc";
      this.Key3Asc.Size = new System.Drawing.Size(104, 17);
      this.Key3Asc.TabIndex = 17;
      this.Key3Asc.TabStop = true;
      this.Key3Asc.Text = "Ascendin&g";
      //
      //Key3
      //
      this.Key3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.Key3.ItemHeight = 13;
      this.Key3.Location = new System.Drawing.Point(16, 32);
      this.Key3.Name = "Key3";
      this.Key3.Size = new System.Drawing.Size(160, 21);
      this.Key3.TabIndex = 16;
      //
      //SortLabel4
      //
      this.SortLabel4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.SortLabel4.Location = new System.Drawing.Point(8, 8);
      this.SortLabel4.Name = "SortLabel4";
      this.SortLabel4.Size = new System.Drawing.Size(288, 16);
      this.SortLabel4.TabIndex = 15;
      this.SortLabel4.Text = "&3rd Key:";
      //
      //SortOK
      //
      this.SortOK.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.SortOK.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.SortOK.Location = new System.Drawing.Point(131, 312);
      this.SortOK.Name = "SortOK";
      this.SortOK.Size = new System.Drawing.Size(80, 24);
      this.SortOK.TabIndex = 20;
      this.SortOK.Text = "OK";
      //
      //SortCancel
      //
      this.SortCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.SortCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.SortCancel.Location = new System.Drawing.Point(217, 312);
      this.SortCancel.Name = "SortCancel";
      this.SortCancel.Size = new System.Drawing.Size(80, 24);
      this.SortCancel.TabIndex = 21;
      this.SortCancel.Text = "Cancel";
      //
      //SortLabel5
      //
      this.SortLabel5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.SortLabel5.Location = new System.Drawing.Point(9, 256);
      this.SortLabel5.Name = "SortLabel5";
      this.SortLabel5.Size = new System.Drawing.Size(288, 16);
      this.SortLabel5.TabIndex = 9;
      //
      //MoveEntire
      //
      this.MoveEntire.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.MoveEntire.Location = new System.Drawing.Point(16, 273);
      this.MoveEntire.Name = "MoveEntire";
      this.MoveEntire.Size = new System.Drawing.Size(168, 24);
      this.MoveEntire.TabIndex = 19;
      this.MoveEntire.Text = "&Move Entire Row";
      //
      //SortDlg
      //
      this.AcceptButton = this.SortOK;
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.BackColor = System.Drawing.SystemColors.Control;
      this.CancelButton = this.SortCancel;
      this.ClientSize = new System.Drawing.Size(306, 344);
      this.Controls.Add(this.MoveEntire);
      this.Controls.Add(this.SortLabel5);
      this.Controls.Add(this.SortCancel);
      this.Controls.Add(this.SortOK);
      this.Controls.Add(this.Panel4);
      this.Controls.Add(this.Panel3);
      this.Controls.Add(this.Panel2);
      this.Controls.Add(this.Panel1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "SortDlg";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Sort";
      this.Panel1.ResumeLayout(false);
      this.Panel2.ResumeLayout(false);
      this.Panel3.ResumeLayout(false);
      this.Panel4.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    // Function to override paint of labels to add a separator bar
    private void Label_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
    {
      // Call common function to paint the separator bar
      common.PaintSeparator(sender, e);
    }


    // Function to set up controls for initial values/text for sorting
    private void AdjustMoveEntire()
    {
      FarPoint.Win.Spread.Model.CellRange cellRange = default(FarPoint.Win.Spread.Model.CellRange);
      int start = 0;
      int count = 0;
      int i = 0;
      string s = null;
      bool enable = false;
      //Dim widthDrop As Integer

      // Set the common text to represent either rows or columns
      if (SortByRows.Checked == true)
      {
        if ((rm != null))
        {
          MoveEntire.Text = rm.GetString("MoveEntire");
        }
        else
        {
          MoveEntire.Text = "&Move Entire Row";
        }
      }
      else
      {
        if ((rm != null))
        {
          MoveEntire.Text = rm.GetString("MoveEntireB");
        }
        else
        {
          MoveEntire.Text = "&Move Entire Column";
        }
      }

      cellRange = SpreadWrapper1.ActiveSheet.GetSelection(0);

#if V5
		if ((cellRange == null)) {
			if ((SpreadWrapper1.ActiveSheet.ActiveCell != null)) {
				cellRange = new FarPoint.Win.Spread.Model.CellRange(SpreadWrapper1.ActiveSheet.ActiveRowIndex, SpreadWrapper1.ActiveSheet.ActiveColumnIndex, 1, 1);
			}
		}
#endif

      // Set up keys initially
      Key1.Items.Clear();
      Key1.Items.Add("(none)");
      Key2.Items.Clear();
      Key2.Items.Add("(none)");
      Key3.Items.Clear();
      Key3.Items.Add("(none)");

      string strLabel = null;
      System.Drawing.Graphics g = null;

      g = System.Drawing.Graphics.FromHwnd(Key1.Handle);

      // Get the column count if sorting by rows
      if (SortByRows.Checked == true)
      {
        if (cellRange.Column == -1)
        {
          start = 0;
          count = SpreadWrapper1.ActiveSheet.NonEmptyColumnCount;
        }
        else
        {
          start = cellRange.Column;
          count = cellRange.ColumnCount;
        }

        // Load the key controls for column selection
        for (i = start; i <= start + count - 1; i++)
        {
          strLabel = SpreadWrapper1.ActiveSheet.GetColumnLabel(0, i);

          if ((strLabel == null) | ((strLabel != null) && strLabel.Length == 0))
          {
            strLabel = i.ToString();
          }

          s = "Column " + strLabel;

          Key1.DropDownWidth = Math.Max(Key1.DropDownWidth, (int)g.MeasureString(s, Key1.Font).Width);

          Key1.Items.Add(s);
          Key2.Items.Add(s);
          Key3.Items.Add(s);
        }

      }
      else
      {
        // Get the row count if sorting by columns
        if (cellRange.Row == -1)
        {
          start = 0;
          count = SpreadWrapper1.ActiveSheet.NonEmptyRowCount;
        }
        else
        {
          start = cellRange.Row;
          count = cellRange.RowCount;
        }

        // Load the key controls for row selection
        for (i = start; i <= start + count - 1; i++)
        {
          strLabel = SpreadWrapper1.ActiveSheet.GetRowLabel(i, 0);

          if ((strLabel == null) | ((strLabel != null) && strLabel.Length == 0))
          {
            strLabel = i.ToString();
          }

          s = "Row " + strLabel;

          Key1.DropDownWidth = Math.Max(Key1.DropDownWidth, (int)g.MeasureString(s, Key1.Font).Width);

          Key1.Items.Add(s);
          Key2.Items.Add(s);
          Key3.Items.Add(s);
        }

      }

      Key2.DropDownWidth = Key1.DropDownWidth;
      Key3.DropDownWidth = Key1.DropDownWidth;

      if ((Key1.Items.Count > 1))
      {
        Key1.SelectedIndex = 1;
      }
      if ((Key2.Items.Count > 0))
      {
        Key2.SelectedIndex = 0;
      }
      if ((Key3.Items.Count > 0))
      {
        Key3.SelectedIndex = 0;
      }
      //Key2.SelectedIndex = -1
      //Key3.SelectedIndex = -1


      // Enable / disable key choices based on row/column count
      if (count <= 1)
        enable = false;
      else
        enable = true;
      Key2.Enabled = enable;
      Key2Asc.Enabled = enable;
      Key2Des.Enabled = enable;

      if (count <= 2)
        enable = false;
      else
        enable = true;
      Key3.Enabled = enable;
      Key3Asc.Enabled = enable;
      Key3Des.Enabled = enable;
    }

    // Function to handle the loading of the form
    private void SortDlg_Load(System.Object sender, System.EventArgs e)
    {
      if ((rm != null))
      {
        this.Text = rm.GetString("SortDlg");
        SortLabel1.Text = rm.GetString("SortLabel1");
        SortByRows.Text = rm.GetString("SortByRows");
        SortByColumns.Text = rm.GetString("SortByColumns");
        SortLabel2.Text = rm.GetString("SortLabel2");
        Key1Des.Text = rm.GetString("Key1Des");
        Key1Asc.Text = rm.GetString("Key1Asc");
        Key2Des.Text = rm.GetString("Key2Des");
        Key2Asc.Text = rm.GetString("Key2Asc");
        SortLabel3.Text = rm.GetString("SortLabel3");
        Key3Des.Text = rm.GetString("Key3Des");
        Key3Asc.Text = rm.GetString("Key3Asc");
        SortLabel4.Text = rm.GetString("SortLabel4");
        SortOK.Text = rm.GetString("SortOK");
        SortCancel.Text = rm.GetString("SortCancel");
        MoveEntire.Text = rm.GetString("MoveEntire");
      }

      // Default to sorting by rows - if columns selected or cells selected
      FarPoint.Win.Spread.Model.CellRange cellRange = default(FarPoint.Win.Spread.Model.CellRange);

      cellRange = SpreadWrapper1.ActiveSheet.GetSelection(0);

#if V5
		if ((cellRange == null)) {
			if ((SpreadWrapper1.ActiveSheet.ActiveCell != null)) {
				cellRange = new FarPoint.Win.Spread.Model.CellRange(SpreadWrapper1.ActiveSheet.ActiveRowIndex, SpreadWrapper1.ActiveSheet.ActiveColumnIndex, 1, 1);
			}
		}
#endif

      if ((cellRange.ColumnCount == -1))
      {
        SortByColumns.Checked = true;
      }
      else
      {
        SortByRows.Checked = true;
      }

      // Following function called by checkedchanged event to initialize controls on the form
      //AdjustMoveEntire()
#if V3
		if ((this.Owner != null))
			this.Icon = this.Owner.Icon;
		// 21877
#endif
    }

    // Function to handle when sorting by rows has changed
    private void SortByRows_CheckedChanged(System.Object sender, System.EventArgs e)
    {
      // Call function to re-initialize controls on the form
      AdjustMoveEntire();
    }

    // Function to handle when sorting by columns has changed
    private void SortByColumns_CheckedChanged(System.Object sender, System.EventArgs e)
    {
      // Call function to re-initialize controls on the form
      AdjustMoveEntire();
    }

    // Function to catch the click event of the OK button
    private void OK_Click(System.Object sender, System.EventArgs e)
    {
      int keyCount = 0;

      // Set up the sort keys based on selections on the form
      if (Key1.SelectedIndex > 0)
      {
        keyCount = 1;
        if (Key2.SelectedIndex > 0)
        {
          keyCount = 2;
          if (Key3.SelectedIndex > 0)
          {
            keyCount = 3;
          }
        }
      }

      // Following code sets up the SortInfo data based on keys and control settings from the form
      if (keyCount > 0)
      {
        FarPoint.Win.Spread.Model.CellRange cellRange = default(FarPoint.Win.Spread.Model.CellRange);
        FarPoint.Win.Spread.SortInfo[] sortInfo = new FarPoint.Win.Spread.SortInfo[keyCount];
        int start = 0;
        int row = 0;
        int column = 0;
        int rowCount = 0;
        int columnCount = 0;

        cellRange = SpreadWrapper1.ActiveSheet.GetSelection(0);

#if V5
			if ((cellRange == null)) {
				if ((SpreadWrapper1.ActiveSheet.ActiveCell != null)) {
					cellRange = new FarPoint.Win.Spread.Model.CellRange(SpreadWrapper1.ActiveSheet.ActiveRowIndex, SpreadWrapper1.ActiveSheet.ActiveColumnIndex, 1, 1);
				}
			}
#endif

        if (SortByRows.Checked == true)
        {
          if (cellRange.Column == -1)
          {
            start = 0;
          }
          else
          {
            start = cellRange.Column;
          }
        }
        else
        {
          if (cellRange.Row == -1)
          {
            start = 0;
          }
          else
          {
            start = cellRange.Row;
          }
        }

        // Create Key 1 SortInfo with selected criteria
        sortInfo[0] = new FarPoint.Win.Spread.SortInfo(Key1.SelectedIndex + start - 1, Key1Asc.Checked, null);
        if (keyCount == 2)
        {
          // Create Key 2 SortInfo with selected criteria
          sortInfo[1] = new FarPoint.Win.Spread.SortInfo(Key2.SelectedIndex + start - 1, Key2Asc.Checked, null);
        }
        if (keyCount == 3)
        {
          // 23117 -scl
          // Create Key 2 SortInfo with selected criteria
          sortInfo[1] = new FarPoint.Win.Spread.SortInfo(Key2.SelectedIndex + start - 1, Key2Asc.Checked, null);
          // Create Key 3 SortInfo with selected criteria
          sortInfo[2] = new FarPoint.Win.Spread.SortInfo(Key3.SelectedIndex + start - 1, Key3Asc.Checked, null);
        }

        // Handle the case for moving entire rows or columns
        row = cellRange.Row;
        column = cellRange.Column;
        rowCount = cellRange.RowCount;
        columnCount = cellRange.ColumnCount;

        if (MoveEntire.Checked)
        {
          if (SortByRows.Checked == true)
          {
            column = -1;
            columnCount = -1;
          }
          else
          {
            row = -1;
            rowCount = -1;
          }
        }
        // Call function in spreadsheet to sort a range of cells based on SortInfo settings
        SpreadWrapper1.ActiveSheet.SortRange(row, column, rowCount, columnCount, SortByRows.Checked, sortInfo);
      }
    }

    private void SortDlg_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
    {
      if ((e.KeyCode == System.Windows.Forms.Keys.F1))
      {
        e.Handled = true;
      }
    }
  } 
}
