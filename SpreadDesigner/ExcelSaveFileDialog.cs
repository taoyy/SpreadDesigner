﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using FarPoint.Excel;

namespace SpreadDesigner
{
  internal class ExcelSaveFileDialog : FileDialogBase
  {
    private CheckedListComboBox comboBoxFlags;
    private int minimizeWidth;
    private SaveFileDialog saveDialog;
    private ExcelSaveFileDialogNative saveFileDialogNative;

    public ExcelSaveFileDialog()
    {
      this.CreateControl();
      this.saveDialog = new SaveFileDialog();
    }

    private void comboBoxFlags_CheckStateChanged(object sender, ItemCheckEventArgs e)
    {
      if (e.Index == 0)
      {
        CheckedListComboBox extendedControl = (CheckedListComboBox)base.ExtendedControl;
        if (e.NewValue == CheckState.Checked)
        {
          extendedControl.DisplayText = common.rm.GetString("ExcelSaveFileDialog.SelectAll");
        }
        else if (e.NewValue == CheckState.Unchecked)
        {
          extendedControl.DisplayText = common.rm.GetString("ExcelSaveFileDialog.comBoxFlags.Text");
        }
        else if (e.NewValue == CheckState.Indeterminate)
        {
          extendedControl.DisplayText = common.rm.GetString("ExcelSaveFileDialog.SelectSome");
        }
      }
    }

    private void CreateControl()
    {
      IEnumerator enumerator = null;
      this.comboBoxFlags = new CheckedListComboBox();
      this.comboBoxFlags.DisplayText = common.rm.GetString("ExcelSaveFileDialog.comBoxFlags.Text");
      List<CheckedListComboBoxItem> array = new List<CheckedListComboBoxItem>();
      try
      {
        enumerator = Enum.GetValues(typeof(ExcelSaveFlags)).GetEnumerator();
        while (enumerator.MoveNext())
        {
          ExcelSaveFlags flags = (ExcelSaveFlags)enumerator.Current;
          if ((flags != ExcelSaveFlags.NoFlagsSet) && (flags != ExcelSaveFlags.UseOOXMLFormat))
          {
            CheckedListComboBoxItem item = new CheckedListComboBoxItem(flags.ToString(), false)
            {
              Tag = flags
            };
            array.Add(item);
          }
        }
      }
      finally
      {
        if (enumerator is IDisposable)
        {
          (enumerator as IDisposable).Dispose();
        }
      }
      CheckedListComboBox.Sort(array);
      this.comboBoxFlags.Items.AddRange(array.ToArray());
      this.comboBoxFlags.CheckStateChanged += new ItemCheckEventHandler(this.comboBoxFlags_CheckStateChanged);
      base.ExtendedControl = this.comboBoxFlags;
    }

    public override void Dispose()
    {
      this.comboBoxFlags.CheckStateChanged -= new ItemCheckEventHandler(this.comboBoxFlags_CheckStateChanged);
      base.Dispose();
    }

    public override FileDialog Dialog
    {
      get
      {
        return this.saveDialog;
      }
    }

    protected override FileDialogBase.FileDialogNative DialogNative
    {
      get
      {
        if (this.saveFileDialogNative == null)
        {
          this.saveFileDialogNative = new ExcelSaveFileDialogNative(base.ExtendedControl, this.IsVistaDialog());
        }
        return this.saveFileDialogNative;
      }
    }

    public ExcelSaveFlags SaveFlags
    {
      get
      {
        IEnumerator enumerator = null;
        ExcelSaveFlags noFlagsSet = ExcelSaveFlags.NoFlagsSet;
        try
        {
          enumerator = this.comboBoxFlags.Items.GetEnumerator();
          while (enumerator.MoveNext())
          {
            CheckedListComboBoxItem current = (CheckedListComboBoxItem)enumerator.Current;
            if ((current.CheckState == CheckState.Checked) && (current.Tag != null))
            {
              noFlagsSet = ((ExcelSaveFlags)current.Tag) | noFlagsSet;
            }
          }
        }
        finally
        {
          if (enumerator is IDisposable)
          {
            (enumerator as IDisposable).Dispose();
          }
        }
        return noFlagsSet;
      }
    }

    internal class ExcelSaveFileDialogNative : FileDialogBase.FileDialogNative
    {
      private int addWidth;
      private IntPtr buttonSaveHandle;
      private IntPtr comboExtentionsHandle;
      private IntPtr comboFileNameHandle;
      private int comboIndex;
      private int minimizeWidth;
      private IntPtr toolBarButtonHandle;

      public ExcelSaveFileDialogNative(Control sourceControl, bool isVistaDialog)
        : base(sourceControl, isVistaDialog)
      {
        this.comboIndex = 0;
      }

      protected override bool FileDialogEnumWindowCallBack(IntPtr hwnd, int lParam)
      {
        StringBuilder param = new StringBuilder(0x100);
        Win32.GetClassName(hwnd, param, param.Capacity);
        switch (Win32.GetDlgCtrlID(hwnd))
        {
          case 1:
            this.buttonSaveHandle = hwnd;
            if (!base.isVistaDialog)
            {
              base.nextControlHandle = Win32.GetWindow(hwnd, 3);
            }
            if (base.controlToSize == null)
            {
              base.controlToSize = new NativeButton(this);
            }
            base.controlToSize.ReleaseHandle();
            base.controlToSize.AssignHandle(hwnd);
            break;
        }
        if (param.ToString() == "ComboBox")
        {
          if (this.comboIndex == 0)
          {
            this.comboFileNameHandle = hwnd;
            this.comboIndex++;
          }
          else if (this.comboIndex == 1)
          {
            this.comboIndex = 0;
          }
        }
        else if ((param.ToString() == "ToolbarWindow32") && (this.toolBarButtonHandle == IntPtr.Zero))
        {
          this.toolBarButtonHandle = hwnd;
          if (base.isVistaDialog)
          {
            base.nextControlHandle = hwnd;
          }
        }
        return true;
      }

      protected internal override void SizingAndMovingAddedControl()
      {
        if (base.sourceControl != null)
        {
          int num2;
          RECT rect3 = new RECT();
          RECT rect2 = new RECT();
          RECT rect = new RECT();
          Point point = new Point();
          Win32.GetClientRect(this.Handle, ref rect3);
          IntPtr hwnd = base.isVistaDialog ? this.toolBarButtonHandle : this.comboFileNameHandle;
          Win32.GetClientRect(hwnd, ref rect2);
          Win32.GetClientRect(this.buttonSaveHandle, ref rect);
          PointStruct lpPoint = new PointStruct((int)rect2.left, (int)rect2.top);
          PointStruct struct4 = new PointStruct((int)rect3.left, (int)rect3.top);
          PointStruct struct2 = new PointStruct((int)rect.left, (int)rect.top);
          Win32.ClientToScreen(this.Handle, ref struct4);
          Win32.ClientToScreen(this.buttonSaveHandle, ref struct2);
          Win32.ClientToScreen(hwnd, ref lpPoint);
          int height = (int)rect.Height;
          if (base.isVistaDialog)
          {
            point = new Point((int)(((lpPoint.x - struct4.x) + rect2.Width) + ((long)8L)), struct2.y - struct4.y);
            num2 = (int)(((struct2.x - lpPoint.x) - rect2.Width) - ((long)20L));
          }
          else
          {
            point = new Point((lpPoint.x - struct4.x) - 1, (lpPoint.y - struct4.y) + ((int)Math.Round((double)(1.4 * rect2.Height))));
            num2 = (int)(rect2.Width + 2L);
          }
          if (height != base.sourceControl.Height)
          {
            base.sourceControl.Height = base.isVistaDialog ? ((int)rect.Height) : ((int)rect2.Height);
            if (base.sourceControl is ComboBox)
            {
              ComboBox sourceControl = (ComboBox)base.sourceControl;
              sourceControl.ItemHeight = (int)Math.Round(Math.Floor((double)(((double)((base.isVistaDialog ? ((int)rect.Height) : ((int)rect2.Height)) * sourceControl.ItemHeight)) / ((double)sourceControl.Height))));
            }
            else
            {
              base.sourceControl.Height = base.isVistaDialog ? ((int)rect.Height) : ((int)rect2.Height);
            }
          }
          if (!point.Equals(base.sourceControl.Location))
          {
            base.sourceControl.Location = point;
          }
          if (num2 != base.sourceControl.Width)
          {
            base.sourceControl.Width = num2;
            this.addWidth = ((int)rect3.Width) - num2;
          }
        }
      }
    }
  }
}

